/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.foo.shape.trapezoid;

import java.awt.*;
import java.awt.geom.GeneralPath;
import org.foo.shape.SimpleShape;

public class Trapezoid implements SimpleShape {

    /**
     * Implements the <tt>SimpleShape.draw()</tt> method for painting the shape.
     *
     * @param g2 The graphics object used for painting.
     * @param p The position to paint the trapezoid.
     **/

    public void draw(Graphics2D g2, Point p) {
        int x = p.x - 25;
        int y = p.y - 12;
        GradientPaint gradient = new GradientPaint(x, y, Color.GREEN, x + 50, y, Color.GREEN);
        g2.setPaint(gradient);

        // Coordinates for the trapezoid
        int[] xcoords = { x + 10, x + 40, x + 50, x };
        int[] ycoords = { y, y, y + 25, y + 25 };

        GeneralPath trapezoid = new GeneralPath(GeneralPath.WIND_EVEN_ODD, xcoords.length);
        trapezoid.moveTo(xcoords[0], ycoords[0]);
        for (int i = 1; i < xcoords.length; i++) {
            trapezoid.lineTo(xcoords[i], ycoords[i]);
        }
        trapezoid.closePath();

        g2.fill(trapezoid);
        BasicStroke wideStroke = new BasicStroke(2.0f);
        g2.setColor(Color.BLACK);
        g2.setStroke(wideStroke);
        g2.draw(trapezoid);
    }
}
