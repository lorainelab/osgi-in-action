package org.foo.shape.octagon;

import java.awt.*;
import java.awt.geom.GeneralPath;
import org.foo.shape.SimpleShape;

import org.foo.shape.SimpleShape;

public class Octagon implements SimpleShape {

  /**
   * Implements the <tt>SimpleShape.draw()</tt> method for painting the shape.
   * 
   * @param g2 The graphics object used for painting.
   * @param p The position to paint the octagon.
   **/
  public void draw(Graphics2D g2, Point p) {
    int centerX = p.x;
    int centerY = p.y;
    int radius = 25;

    GradientPaint gradient = new GradientPaint(centerX - radius, centerY - radius, Color.CYAN, centerX + radius, centerY + radius, Color.WHITE);
    g2.setPaint(gradient);

    int[] xcoords = new int[8];
    int[] ycoords = new int[8];

    for (int i = 0; i < 8; i++) {
      double angle = Math.toRadians(22.5 + (45 * i));
      xcoords[i] = centerX + (int) (radius * Math.cos(angle));
      ycoords[i] = centerY + (int) (radius * Math.sin(angle));
    }

    GeneralPath polygon = new GeneralPath(GeneralPath.WIND_EVEN_ODD, xcoords.length);
    polygon.moveTo(xcoords[0], ycoords[0]);
    for (int i = 1; i < xcoords.length; i++) {
      polygon.lineTo(xcoords[i], ycoords[i]);
    }
    polygon.closePath();
    g2.fill(polygon);
    BasicStroke wideStroke = new BasicStroke(2.0f);
    g2.setColor(Color.black);
    g2.setStroke(wideStroke);
    g2.draw(polygon);

  }
}
